import type { PlayerId } from 'modules/player';

export type Auth = {
  id: PlayerId,
};
