// @flow strict

import React, {
  createContext,
  useState,
  useContext,
  useMemo,
  type Node,
  useEffect,
} from 'react';

import type { Player } from 'modules/player';
import { MapStream, MapStore, type Map, type MapStreamType } from 'modules/map';

type MapActions = {
  addPlayer: (player: Player) => void,
};

export const MapStreamContext = createContext<MapStreamType>();
export const MapActionsContext = createContext<?MapActions>();

export function MapContainer({ children }: { children: Node }): Node {
  const mapStream = useMemo(() => {
    const stream = new MapStream();

    stream.fill();

    return stream;
  }, []);

  const actions = useMemo(
    () => ({
      addPlayer(player) {
        mapStream.addPlayer(player);
      },
    }),
    [mapStream],
  );

  return (
    <MapStreamContext.Provider value={mapStream.get()}>
      <MapActionsContext.Provider value={actions}>
        {children}
      </MapActionsContext.Provider>
    </MapStreamContext.Provider>
  );
}

export function useMapStream(): MapStreamType {
  const stream = useContext(MapStreamContext);

  return stream;
}

export function useMapState(): Map {
  const stream = useMapStream();
  const [state, setState] = useState(() => new MapStore().fill());

  useEffect(() => {
    const subscribtion = stream.subscribe(setState);

    return () => subscribtion.unsubscribe();
  }, [stream]);

  return state;
}

export function useMapActions(): ?MapActions {
  const actions = useContext(MapActionsContext);

  return actions;
}

export function useMap() {
  const state = useMapState();
  const actions = useMapActions();

  return [state, actions];
}
