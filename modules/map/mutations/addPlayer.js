// @flow strict

import type { Player } from 'modules/player';

import { type Map } from '../types';
import { getRandomEmptyCoords } from '../utils';
import { movePlayer } from './movePlayer';

export function addPlayer(map: Map, player: Player): Map {
  const coords = getRandomEmptyCoords(map);

  return movePlayer(map, player, coords);
}
