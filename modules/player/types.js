export type PlayerId = string;
export type PlayerName = sting;

export type Player = {
  name: PlayerName,
  id: PlayerId,
};
