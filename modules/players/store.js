// @flow strict

import type { Players } from './types';
import type { Player } from 'modules/player';

export class PlayersStore {
  players: Players;

  constructor(players: Players): PlayersStore {
    this.players = players;

    return this;
  }

  add(player: Player): PlayersStore {
    return this.update([...this.raw(), player]);
  }

  delete(player: Player): PlayersStore {
    return this.update(this.raw().filter((p) => p.id !== player.id));
  }

  update(players: Players): PlayersStore {
    return new PlayersStore(players);
  }

  raw(): Players {
    return this.players;
  }
}
