// @flow strict

import type { Auth } from './types';
import type { PlayerId } from 'modules/player';

export class AuthStore {
  id: PlayerId | null;

  constructor(id: PlayerId | null): AuthStore {
    this.id = id;

    return this;
  }

  raw(): Auth {
    return { id: this.id };
  }
}
