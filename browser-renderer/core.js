import React, { useEffect, useState } from 'react';

import { Map } from './Map';

import { PlayerStore } from 'modules/player';
import { usePlayers } from './storage/players';
import { useMap } from './storage/map';
import { useAuth } from './storage/auth';

import { useKeyboardStream } from './hooks';

export function Core() {
  const [players, { add }] = usePlayers();
  const [, { addPlayer }] = useMap();
  const [, { login }] = useAuth();

  const keyboardStream = useKeyboardStream();

  useEffect(() => {
    const player = new PlayerStore({
      name: 'tim',
    });

    add(player);
    addPlayer(player);
    login(player);
  }, []);

  useEffect(() => {
    // players.forEach((player) => {});
  }, [players]);

  useEffect(() => {
    keyboardStream.subscribe(({ key }) => {
      console.warn(key);
    });
  }, [keyboardStream]);

  return <Map players={players} />;
}
