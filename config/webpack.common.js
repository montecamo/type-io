const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const rootDir = path.resolve(__dirname, '../');

let config = {
  context: rootDir,

  entry: {
    app: './browser-renderer/index.js',
  },

  resolve: {
    modules: [rootDir, 'node_modules'],
    extensions: ['.js'],
  },

  output: {
    filename: 'main.js',
    path: path.join(__dirname, '../dist'),
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, '../public/index.html'),
    }),
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'astroturf/loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'astroturf/css-loader'],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.(woff(2)?|ttf|eot|jpg|ico)(\?v=\d+\.\d+\.\d+)?$/,
        use: ['file-loader?name=[name].[ext]'],
      },
    ],
  },
};

module.exports = config;
