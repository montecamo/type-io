import { useMemo } from 'react';

import { getPlayerPosition, type Coordinates } from 'modules/map';
import { useMap } from '../storage/map';
import { useAuth } from '../storage/auth';

export function usePlayerPosition(): Coordinates {
  const [map] = useMap();
  const [auth] = useAuth();

  return useMemo(() => {
    return getPlayerPosition(map, auth.id);
  }, [map, auth]);
}
