import { useMemo, useEffect } from 'react';

import {
  getPlayerPosition,
  getAvailableMoves,
  type Coordinates,
} from 'modules/map';
import { useMap } from '../storage/map';
import { useAuth } from '../storage/auth';
import { useKeyboardStream } from './useKeyboardStream';

export function usePlayerMove(): Coordinates[] {
  const [map] = useMap();
  const [auth] = useAuth();

  const keyboardStream = useKeyboardStream();

  useEffect(() => {
    keyboardStream.subscribe(({ key }) => {
      console.warn(key);
    });
  }, [keyboardStream]);

  return useMemo(() => {
    const playerCoords = getPlayerPosition(map, auth.id);

    console.warn('coords', playerCoords);
    const moves = playerCoords
      ? getAvailableMoves(playerCoords, map)
      : undefined;

    return moves;
  }, [map, auth]);
}
