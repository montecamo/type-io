// @flow strict

import React, { type Element } from 'react';

import styled from 'astroturf';

type CellProps = {
  letter: string,
  color: string,
  isArea: boolean,
  isPath: boolean,
  isMove: boolean,
  isPlayer: boolean,
};

const StyledCell = styled.div`
  color: var(--cell-default-color);
  background: var(--cell-default-background);
  opacity: var(--cell-default-opacity);

  border: 1px solid transparent;

  width: var(--cell-size, 75px);
  height: var(--cell-size, 75px);

  display: flex;
  justify-content: center;
  align-items: center;

  box-sizing: border-box;

  &.area {
    opacity: 1;
  }

  &.path {
    opacity: 0.6;
  }

  &.player {
    background: red;
  }

  &.move {
    background: lightblue;
  }
`;

export function Cell({
  isArea,
  isPath,
  letter,
  isPlayer,
  isMove,
}: CellProps): Element<StyledCell> {
  return (
    <StyledCell area={isArea} path={isPath} player={isPlayer} move={isMove}>
      {letter}
    </StyledCell>
  );
}
