// @flow strict

import React from 'react';
import { render } from 'react-dom';

import { Core } from './Core';
import { MapContainer } from './storage/map';
import { PlayersContainer } from './storage/players';
import { AuthContainer } from './storage/auth';

function App() {
  return (
    <AuthContainer>
      <MapContainer>
        <PlayersContainer>
          <Core />
        </PlayersContainer>
      </MapContainer>
    </AuthContainer>
  );
}

render(<App />, document.getElementById('root'));
