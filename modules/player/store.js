// @flow strict

// $FlowFixMe
import { nanoid } from 'nanoid';

import type { Player, PlayerName, PlayerId } from './types';

function defaultPlayer(name: PlayerName): Player {
  return {
    name,
    id: nanoid(),
  };
}

export class PlayerStore {
  name: PlayerName;
  id: PlayerId;

  constructor(name: PlayerName): Player {
    Object.assign(this, defaultPlayer(name));

    return this;
  }

  raw() {
    return {
      name: this.name,
      id: this.id,
    };
  }
}
