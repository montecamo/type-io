// @flow strict

import type { Map, Coordinates, Cell } from 'modules/map/types';

import { getCell } from 'modules/map/utils';
import { findCell } from './map';

const moveTop = (coords: Coordinates): Coordinates => ({
  ...coords,
  y: coords.y - 1,
});
const moveBottom = (coords: Coordinates): Coordinates => ({
  ...coords,
  y: coords.y + 1,
});
const moveLeft = (coords: Coordinates): Coordinates => ({
  ...coords,
  x: coords.x - 1,
});
const moveRight = (coords: Coordinates): Coordinates => ({
  ...coords,
  x: coords.x + 1,
});

export function getAvailableMoves(
  coords: Coordinates,
  map: Map,
  predicate: (Cell) => boolean = () => true,
): Coordinates[] {
  const availablePositions = [
    moveTop(coords),
    moveRight(coords),
    moveBottom(coords),
    moveLeft(coords),
  ];

  return availablePositions.filter(({ x, y }) => {
    return (
      x >= 0 &&
      y >= 0 &&
      x < map.width &&
      y < map.height &&
      predicate(getCell(x, y, map))
    );
  });
}

export function getPlayerPosition(map: Map, playerId: string): Coords | void {
  const cell = findCell(
    map,
    (cell) => cell.playerId === playerId && cell.state === 'player',
  );

  if (!cell) return undefined;

  return { x: cell.x, y: cell.y };
}
