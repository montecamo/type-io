// @flow strict

import { generateLetterMap } from 'modules/lettermap';
import { type Observable, BehaviorSubject } from 'rxjs';

import type { Player } from 'modules/player';
import type { Map, Width, Height, Cells } from './types';

import { fillMap } from './utils';

import { addPlayer } from './mutations';

export type MapStreamType = Observable<Map>;

function defaultMap(): Map {
  return { width: 10, height: 10, cells: [] };
}

export class MapStore {
  width: Width;
  height: Height;
  cells: Cells;

  constructor(map: Map = defaultMap()) {
    Object.assign(this, map);

    return this;
  }

  // $FlowFixMe
  fill(generator = generateLetterMap) {
    return this.update(fillMap(this.raw(), generator));
  }

  addPlayer(player: Player) {
    return this.update(addPlayer(this.raw(), player));
  }

  update(map: Map): MapStore | void {
    return new MapStore(map);
  }

  raw(): Map {
    return {
      width: this.width,
      height: this.height,
      cells: this.cells,
    };
  }
}

export class MapStream extends MapStore {
  stream: MapStreamType;

  constructor(map: Map) {
    super(map);

    this.stream = new BehaviorSubject(this.raw());

    return this;
  }

  update(map: Map) {
    this.stream.next(map);
  }

  get(): Observable<Map> {
    return this.stream;
  }
}
