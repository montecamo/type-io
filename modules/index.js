// @flow strict

import React from 'react';
import { render } from 'react-dom';

import { css } from 'astroturf';

import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';

const styles = css`
  .button {
    color: black;
    border: 1px solid black;
    background-color: white;
  }
`;

const Test = () => {
  return <button className={styles.button}>hello world from react</button>;
};

render(<Test />, document.getElementById('root'));
