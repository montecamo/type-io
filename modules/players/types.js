import type { Player } from 'modules/player';

export type Players = Player[];
