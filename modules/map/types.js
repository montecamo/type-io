// @flow strict

export type Cell = {
  playerId?: string,
  letter: string,
  state: 'empty' | 'path' | 'area' | 'player',
  x: number,
  y: number,
};

type Column = Cell[];

export type Cells = Column[];
export type Width = number;
export type Height = number;

export type Map = {
  cells: Cells,
  width: Width,
  height: Height,
};

export type Coordinates = {
  x: number,
  y: number,
};
