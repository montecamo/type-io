import { useMemo } from 'react';

import { getAvailableMoves, type Coordinates } from 'modules/map';
import { usePlayerPosition } from './usePlayerPosition';

import { useMap } from '../storage/map';

export function useAvailableMoves(): Coordinates[] {
  const [map] = useMap();

  const position = usePlayerPosition();

  return useMemo(() => {
    const moves = position ? getAvailableMoves(position, map) : undefined;

    return moves;
  }, [position, map]);
}
