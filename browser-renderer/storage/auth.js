// @flow strict

import React, {
  createContext,
  useState,
  useContext,
  useMemo,
  type Node,
} from 'react';

import { AuthStore } from 'modules/auth';
import type { Player } from 'modules/player';

type AuthActions = {
  login(player: Player): void,
};

export const AuthStateContext = createContext<AuthStore>();
export const AuthActionsContext = createContext<?AuthActions>();

function useActions(update: (AuthStore) => void): AuthActions {
  const actions = useMemo(
    () => ({
      login(player) {
        const authStore = new AuthStore(player.id);

        update(authStore);
      },
    }),
    [update],
  );

  return actions;
}

export function AuthContainer({ children }: { children: Node }): Node {
  const [authState, updateAuthState] = useState(() => new AuthStore(null));

  const actions = useActions(updateAuthState);

  return (
    <AuthStateContext.Provider value={authState.raw()}>
      <AuthActionsContext.Provider value={actions}>
        {children}
      </AuthActionsContext.Provider>
    </AuthStateContext.Provider>
  );
}

export function useAuthState(): AuthStore {
  const store = useContext(AuthStateContext);

  if (!store) {
    throw new Error('useAuthAction() should be used inside AuthActionsContext');
  }

  return store;
}

export function useAuthActions(): AuthActions {
  const actions = useContext(AuthActionsContext);

  if (!actions) {
    throw new Error('useAuthAction() should be used inside AuthActionsContext');
  }

  return actions;
}

export function useAuth() {
  const store = useAuthState();
  const actions = useAuthActions();

  return [store, actions];
}
