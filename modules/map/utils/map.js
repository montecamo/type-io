// @flow strict

import { produce } from 'immer';

import { getRandomNumber } from 'modules/utils';

import { type Map, type Cells, type Cell, type Coordinates } from 'modules/map';

export function getEmptyCells(map: Map): Cells {
  return map.cells.map((column) =>
    column.filter((cell) => cell.state === 'empty'),
  );
}

export function getCell(x: number, y: number, map: Map): Cell {
  return map.cells[y][x];
}

export function updateCell(
  map: Map,
  { x, y }: Coordinates,
  change: (cell: Cell) => Cell,
): Map {
  return produce(map, (draft) => {
    draft.cells[y][x] = change(draft.cells[y][x]);
  });
}

export function fillMap(map: Map, generate: (number, number) => string[][]) {
  const generated = generate(map.width, map.height);

  return {
    ...map,
    cells: generated.map<Cell[]>((row, y) =>
      row.map<Cell>((letter, x) => ({ letter, state: 'empty', y, x })),
    ),
  };
}

export function findCell(map: Map, match: (Cell) => boolean) {
  const flat = map.cells.flatMap((c) => c);

  return flat.find(match);
}

export function getRandomEmptyCoords(
  map: Map,
  emptyCellsGetter: (Map) => Cells[] = getEmptyCells,
  randomNumberGetter: (number, number) => number = getRandomNumber,
): Coordinates {
  const emptyCells = emptyCellsGetter(map);

  console.warn('empty', emptyCells);
  const randomX = randomNumberGetter(0, emptyCells.length - 1);
  const randomY = randomNumberGetter(0, emptyCells[randomX].length - 1);

  return { x: randomX, y: randomY };
}
