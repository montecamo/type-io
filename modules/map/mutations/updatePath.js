// @flow strict

import type { Player } from 'modules/player';

import { type Map, type Cell, type Coordinates } from '../types';
import { updateCell } from '../utils';

export function updatePath(map: Map, player: Player, coords: Coordinates): Map {
  return updateCell(map, coords, (cell: Cell): Cell => ({
    ...cell,
    playerId: player.id,
    state: 'path',
  }));
}
