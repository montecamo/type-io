// @flow strict

import { alphabet } from './alphabet';

export function generateLetterMap(width: number, height: number): string[][] {
  const map = [];

  let letterPointer = 0;

  for (let i = 0; i < height; i++) {
    map[i] = [];

    for (let j = 0; j < width; j++) {
      map[i][j] = alphabet[letterPointer];

      letterPointer++;

      letterPointer = letterPointer % alphabet.length;
    }
  }

  return map;
}
