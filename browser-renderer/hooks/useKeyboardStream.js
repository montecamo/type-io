import { useState } from 'react';
import { fromEvent } from 'rxjs';

export function useKeyboardStream() {
  const [stream] = useState(fromEvent(document, 'keydown'));

  return stream;
}
