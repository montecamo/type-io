// @flow strict

import React, {
  createContext,
  useState,
  useContext,
  useMemo,
  useCallback,
  type Node,
} from 'react';

import { PlayersStore } from 'modules/players';
import type { Player } from 'modules/player';

type AuthActions = {
  add(Player): void,
};

export const PlayersStateContext = createContext<PlayersStore>();
export const PlayersActionsContext = createContext<?AuthActions>();

type StateUpdater = (PlayersStore) => PlayersStore;

function useActions(update: (StateUpdater) => void): AuthActions {
  const actions = useMemo(
    () => ({
      add(player: Player): PlayersStore {
        update((players) => players.add(player));
      },
    }),
    [update],
  );

  return actions;
}

export function PlayersContainer({ children }: { children: Node }): Node {
  const [players, updatePlayers] = useState(() => new PlayersStore([]));

  const commitUpdate = useCallback(
    (update) => {
      updatePlayers(update(players));
    },
    [players, updatePlayers],
  );

  const actions = useActions(commitUpdate);

  return (
    <PlayersStateContext.Provider value={players.raw()}>
      <PlayersActionsContext.Provider value={actions}>
        {children}
      </PlayersActionsContext.Provider>
    </PlayersStateContext.Provider>
  );
}

export function usePlayers() {
  const store = useContext(PlayersStateContext);
  const actions = useContext(PlayersActionsContext);

  return [store, actions];
}
