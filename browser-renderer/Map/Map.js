// @flow strict

import React from 'react';
import styled from 'astroturf';

import { Cell } from './Cell';
import type { Coordinates } from 'modules/map';

import { useMap } from '../storage/map';
import { useAvailableMoves } from '../hooks';

const CellsRow = styled.div`
  display: flex;
`;

function isMove(moves: Coordinates[], cell: Cell): boolean {
  return Boolean(moves.find((move) => move.x === cell.x && move.y === cell.y));
}

export function Map() {
  const [map] = useMap();
  const availableMoves = useAvailableMoves();

  console.warn('moved', availableMoves);

  return map.cells.map((row) => (
    <CellsRow>
      {row.map((cell) => (
        <Cell
          letter={cell.letter}
          isPlayer={cell.state === 'player'}
          isMove={isMove(availableMoves ?? [], cell)}
        />
      ))}
    </CellsRow>
  ));
}
